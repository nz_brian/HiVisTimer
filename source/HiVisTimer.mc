using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Graphics as Graphics;
using Toybox.System as System;

//! @author Brian Bannister
class HiVisTimer extends App.AppBase {
	hidden var view;
	
	function initialize() {
        AppBase.initialize();
    }

    function getInitialView() {
        view = new HiVisTimerView();
        onSettingsChanged();
        return [ view ];
    }
    
    
    function onSettingsChanged() { 
    	System.println("onSettingsChanged()");
    }
}


//! @author Brian Bannister
class HiVisTimerView extends Ui.DataField {

    hidden const RIGHT = Graphics.TEXT_JUSTIFY_RIGHT | Graphics.TEXT_JUSTIFY_VCENTER;
    hidden const LEFT = Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER;
    hidden const CENTER = Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER;
    
    hidden const HEADER_FONT = Graphics.FONT_XTINY;
    hidden const VALUE_FONT = Graphics.FONT_NUMBER_HOT;
    hidden const LARGE_FONT = Graphics.FONT_NUMBER_THAI_HOT;
    
    hidden const ZERO_TIME = "0:00";
    
    hidden const RED = Graphics.COLOR_RED;
    hidden const YELLOW = Graphics.COLOR_YELLOW;
    hidden const GREEN = Graphics.COLOR_GREEN;
    hidden const DK_GREEN = Graphics.COLOR_DK_GREEN;
    hidden const BLUE = Graphics.COLOR_BLUE;
    hidden const WHITE = Graphics.COLOR_WHITE;
    hidden const BLACK = Graphics.COLOR_BLACK;
    hidden const GRAY = Graphics.COLOR_LT_GRAY;
    hidden const DK_GRAY = Graphics.COLOR_DK_GRAY;
    hidden const TRANSPARENT = Graphics.COLOR_TRANSPARENT;
    
    hidden var textColor = BLACK;
    hidden var inverseTextColor = WHITE;
    hidden var backgroundColor = WHITE;
    hidden var inverseBackgroundColor = BLACK;
    hidden var headerColor = DK_GRAY;
    
    hidden var elapsedTime = 0;
    
    function initialize() {
        DataField.initialize();
    }

    function compute(info) {      
        elapsedTime = info.elapsedTime != null ? info.elapsedTime : 0; 
    }
    
    function onLayout(dc) {
        onUpdate(dc);
    }
    
    function onUpdate(dc) {
        setColors();
        // reset background
        dc.setColor(backgroundColor, backgroundColor);
        dc.fillRectangle(0, 0, 218, 218);
        
        drawValues(dc);
    }
     
    function drawValues(dc) {
        
        var duration;
        var hours = null;
        if (elapsedTime != null && elapsedTime > 0) {
            var minutes = elapsedTime / 1000 / 60;
            var seconds = elapsedTime / 1000 % 60;
            
            if (minutes >= 60) {
                hours = minutes / 60;
                minutes = minutes % 60;
            }
            
            //hours = 99;
            
            if (hours == null || 0 == hours) {
                duration = minutes.format("%d") + ":" + seconds.format("%02d");
            } else {
                duration = hours.format("%d") + ":" + minutes.format("%02d") + ":" + seconds.format("%02d");
            }
        } else {
            duration = ZERO_TIME;
        } 
        dc.setColor(textColor, TRANSPARENT);
    	dc.drawText(200, 38, Graphics.FONT_NUMBER_THAI_HOT, duration, RIGHT);
    	
       	dc.setColor(headerColor, TRANSPARENT);
        if (null == hours || 0 == hours) {      	
        	dc.drawText(4, 32, Graphics.FONT_LARGE, "Timer", LEFT);
        }
    }
    
    function setColors() {
    	var colourSetting = getBackgroundColor();
        if (null != colourSetting && colourSetting != backgroundColor) {
            backgroundColor = colourSetting;
            textColor = (backgroundColor == BLACK) ? WHITE : BLACK;
            inverseTextColor = (backgroundColor == BLACK) ? WHITE : WHITE;
            inverseBackgroundColor = (backgroundColor == BLACK) ? DK_GRAY: BLACK;
            headerColor = (backgroundColor == BLACK) ? GRAY: DK_GRAY;
        }
    }
   
}
